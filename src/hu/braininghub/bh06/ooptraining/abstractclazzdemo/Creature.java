package hu.braininghub.bh06.ooptraining.abstractclazzdemo;

public interface Creature {
	
	void kill();
}
