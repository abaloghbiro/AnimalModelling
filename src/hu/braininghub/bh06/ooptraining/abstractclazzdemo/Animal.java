package hu.braininghub.bh06.ooptraining.abstractclazzdemo;

public interface Animal extends Creature {

	void move();
	
	void eat();
	
	Animal proCreate();
}
