package hu.braininghub.bh06.ooptraining.abstractclazzdemo;

public class RoboDog extends Dog implements Robot {

	private Double currentEnergyLevel = new Double(0.0d);

	public RoboDog(String species, String sex, int age) {
		super(species, sex, age);
	}

	@Override
	public void charge() {
		currentEnergyLevel += 1.0;
		System.out.println("Charging..." + currentEnergyLevel);

	}

	@Override
	public void shutdown() {
		System.exit(0);
	}

	@Override
	public void start() {
		System.out.println("Started...");
	}

	@Override
	public void executeCommand(String command) {
		System.out.println("Command executed: " + command);

	}

}
