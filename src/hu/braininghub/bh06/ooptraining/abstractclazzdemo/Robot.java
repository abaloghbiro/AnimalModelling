package hu.braininghub.bh06.ooptraining.abstractclazzdemo;

public interface Robot {
	void charge();
	void shutdown();
	void start();
	void executeCommand(String command);
}
