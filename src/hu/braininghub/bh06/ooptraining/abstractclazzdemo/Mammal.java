package hu.braininghub.bh06.ooptraining.abstractclazzdemo;

public abstract class Mammal extends AbstractAnimal {

	public Mammal(String species, String sex, int age, boolean isCarnivore, int numOfLegs) {
		super(species, sex, age, isCarnivore, numOfLegs);
	}

	@Override
	public void move() {
		System.out.println("Mammal is running...");
	}
}
