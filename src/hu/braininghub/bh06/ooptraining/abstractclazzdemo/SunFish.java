package hu.braininghub.bh06.ooptraining.abstractclazzdemo;

public class SunFish extends Fish{

	public SunFish(String sex, int age) {
		super("sunfish", sex, age, false);
	}

	@Override
	public void eat() {
		System.out.println("Sun fish eats plants...");
	}

	@Override
	public SunFish proCreate() {
		return new SunFish("male", 1);
	}

	@Override
	public String toString() {
		return "SunFish [asasas]";
	}
	
	
}
