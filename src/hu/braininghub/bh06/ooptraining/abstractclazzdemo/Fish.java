package hu.braininghub.bh06.ooptraining.abstractclazzdemo;

public abstract class Fish extends AbstractAnimal {

	public Fish(String species, String sex, int age, boolean isCarnivore) {
		super(species, sex, age, isCarnivore, 0);
	}

	@Override
	public void move() {
		System.out.println("Fish is swimming...");
	}

}
