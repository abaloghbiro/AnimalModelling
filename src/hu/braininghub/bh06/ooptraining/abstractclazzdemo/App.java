package hu.braininghub.bh06.ooptraining.abstractclazzdemo;

public class App {

	public static void main(String[] args) {

		SunFish step1 = new SunFish("male", 1);

		Fish step2 = step1;

		AbstractAnimal step3 = step2;

		Animal step4 = step3;

		Creature step5 = step4;
		
		System.out.println(step5);
		
		
		Robot r = new RoboDog("steel", "male", 120);
		
		r.charge();
		
		r.executeCommand("kill -9");
		r.shutdown();
		
		System.out.println("------------------------");
	}
}
