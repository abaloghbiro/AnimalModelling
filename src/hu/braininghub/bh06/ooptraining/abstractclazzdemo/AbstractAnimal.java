package hu.braininghub.bh06.ooptraining.abstractclazzdemo;

public abstract class AbstractAnimal implements Animal {

	private String species;
	private String sex;
	private int age;
	private boolean isCarnivore;
	private int numOfLegs;
	private boolean isAlive = true;
	
	
	public AbstractAnimal(String species, String sex, int age, boolean isCarnivore, int numOfLegs) {
		this.species = species;
		this.sex = sex;
		this.age = age;
		this.isCarnivore = isCarnivore;
		this.numOfLegs = numOfLegs;
	}
	
	
	@Override
	public void kill() {
		this.isAlive = false;
	}
	
	public boolean isAlive() {
		return isAlive;
	}

	public String getSpecies() {
		return species;
	}
	public void setSpecies(String species) {
		this.species = species;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public boolean isCarnivore() {
		return isCarnivore;
	}
	public void setCarnivore(boolean isCarnivore) {
		this.isCarnivore = isCarnivore;
	}
	public int getNumOfLegs() {
		return numOfLegs;
	}
	public void setNumOfLegs(int numOfLegs) {
		this.numOfLegs = numOfLegs;
	}
	
	
	
	
}
