package hu.braininghub.bh06.ooptraining.abstractclazzdemo;

public class Dog extends Mammal {

	public Dog(String species, String sex, int age) {
		super(species, sex, age, true, 4);
	}

	@Override
	public void eat() {
		System.out.println("Dog eats cats");
	}

	@Override
	public Animal proCreate() {
		return new Dog(getSpecies(), getSex(), 0);
	}
}
